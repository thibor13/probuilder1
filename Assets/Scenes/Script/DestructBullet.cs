﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructBullet : MonoBehaviour
{
    private void Update()
    {
        Destroy(this.gameObject, 2f);
    }
    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "AmmoDestruct")
        {
            Destroy(this.gameObject);
        }
    }
}
